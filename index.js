function checkPrime(e) {
  if (e < 2) return false;
  for (var t = 2; t < e; t++) if (e % t == 0) return false;
  return true;
}
function printPrime() {
  var numP = +document.getElementById("inputN2").value;
  for (var t = "", n = 1; n <= numP; n++) {
    if (checkPrime(n) == true) {
      t += " " + n;
    }
  }
  document.getElementById("txtResult5").innerHTML = t;
}
